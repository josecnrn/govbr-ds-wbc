import { action } from '@storybook/addon-actions'
import { kebabiseArgs } from '../../util/Utils.js'
import brSelect from './Select.ce.vue'

export default {
  title: 'Componentes/Select',
  component: brSelect,
  argTypes: {
    label: {
      control: 'text',
      defaultValue: null,
    },
    multiple: {
      control: 'boolean',
      defaultValue: false,
    },
    placeholder: {
      control: 'text',
      defaultValue: null,
    },
    options: {
      control: 'array',
      defaulValue: null,
    },
    showSearchIcon: {
      control: 'boolean',
      default: false,
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-select v-bind="args" @change="handleMultiple"></br-select>`,
  methods: { handleMultiple: action('change') },
})

export const Simples = Template.bind({})
Simples.args = {
  label: 'Estados Brasileiros',
  placeholder: 'Selecione o Estado',
  options: [
    {
      value: 'Acre',
      selected: false,
    },
    {
      value: 'Alagoas',
      selected: false,
    },
    {
      value: 'Amapá',
      selected: false,
    },
    {
      value: 'Amazonas',
      selected: false,
    },
    {
      value: 'Bahia',
      selected: false,
    },
    {
      value: 'Ceará',
      selected: false,
    },
    {
      value: 'Distrito Federal',
      selected: false,
    },
    {
      value: 'Espírito Santo',
      selected: false,
    },
    {
      value: 'Goiás',
      selected: false,
    },
    {
      value: 'Maranhão',
      selected: false,
    },
    {
      value: 'Mato Grosso',
      selected: false,
    },
    {
      value: 'Mato Grosso do Sul',
      selected: false,
    },
    {
      value: 'Minas Gerais',
      selected: false,
    },
    {
      value: 'Pará',
      selected: false,
    },
    {
      value: 'Paraíba',
      selected: false,
    },
    {
      value: 'Paraná',
      selected: false,
    },
    {
      value: 'Pernambuco',
      selected: false,
    },
    {
      value: 'Piauí',
      selected: false,
    },
    {
      value: 'Rio de Janeiro',
      selected: false,
    },
    {
      value: 'Rio Grande do Norte',
      selected: false,
    },
    {
      value: 'Rio Grande do Sul',
      selected: false,
    },
    {
      value: 'Rondônia',
      selected: false,
    },
    {
      value: 'Roraima',
      selected: false,
    },
    {
      value: 'Santa Catarina',
      selected: false,
    },
    {
      value: 'São Paulo',
      selected: false,
    },
    {
      value: 'Sergipe',
      selected: false,
    },
    {
      value: 'Tocantins',
      selected: false,
    },
  ],
}

export const Multiplo = Template.bind({})
Multiplo.args = {
  label: 'Estados Brasileiros',
  placeholder: 'Selecione o Estado',
  multiple: true,
  options: `[
    {
      "value": "Acre",
      "selected": false
    },
    {
      "value": "Alagoas",
      "selected": false
    },
    {
      "value": "Amapá",
      "selected": false
    },
    {
      "value": "Amazonas",
      "selected": false
    },
    {
      "value": "Bahia",
      "selected": false
    },
    {
      "value": "Ceará",
      "selected": false
    },
    {
      "value": "Distrito Federal",
      "selected": false
    },
    {
      "value": "Espírito Santo",
      "selected": false
    },
    {
      "value": "Goiás",
      "selected": false
    },
    {
      "value": "Maranhão",
      "selected": false
    },
    {
      "value": "Mato Grosso",
      "selected": false
    },
    {
      "value": "Mato Grosso do Sul",
      "selected": false
    },
    {
      "value": "Minas Gerais",
      "selected": false
    },
    {
      "value": "Pará",
      "selected": false
    },
    {
      "value": "Paraíba",
      "selected": false
    },
    {
      "value": "Paraná",
      "selected": false
    },
    {
      "value": "Pernambuco",
      "selected": false
    },
    {
      "value": "Piauí",
      "selected": false
    },
    {
      "value": "Rio de Janeiro",
      "selected": false
    },
    {
      "value": "Rio Grande do Norte",
      "selected": false
    },
    {
      "value": "Rio Grande do Sul",
      "selected": false
    },
    {
      "value": "Rondônia",
      "selected": false
    },
    {
      "value": "Roraima",
      "selected": false
    },
    {
      "value": "Santa Catarina",
      "selected": false
    },
    {
      "value": "São Paulo",
      "selected": false
    },
    {
      "value": "Sergipe",
      "selected": false
    },
    {
      "value": "Tocantins",
      "selected": false
    }
  ]`,
}
